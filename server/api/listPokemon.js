const { aggregateFromCollection } = require('../middleware/db');
const router = require('express').Router();

router.get('/', async (req, res)=>{
    const { num, page } = req.query;
    const aggregateQuery = [
        {
          '$project': {
            '_id': 0, 
            'id': 1, 
            'species_id': 1, 
            'name': 1,
            'types': 1
          }
        },
        {
            '$sort': {
                'id': 1
            }
        },
        {
            '$facet': {
                'results': [
                    {
                        '$skip': Math.max((+page-1)||0, 0)*Math.max(+num||20, 10) 
                    }, {
                        '$limit': Math.max(+num||20, 10) 
                    }
                ], 
                'totalCount': [
                    {
                        '$count': 'count'
                    }
                ]
            }
        }, {
            '$project': {
                'results': 1, 
                'totalCount': {
                    '$arrayElemAt': [
                        '$totalCount.count', 0
                    ]
                }, 
                'totalPage': {
                    '$ceil': {
                        '$divide': [
                            {
                                '$arrayElemAt': [
                                '$totalCount.count', 0
                                ]
                            }, Math.max(+num||20, 10) 
                        ]
                    }
                },
                'currentPage': {
                    '$abs': Math.max(+page||1, 1)
                },
                'num': {
                    '$abs': Math.max(+num||20, 10) 
                },
                
            }
        }
    ];
    const data = await aggregateFromCollection('format', aggregateQuery);

    res.json(data[0]);
})

module.exports = router;