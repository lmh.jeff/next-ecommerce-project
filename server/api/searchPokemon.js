const { aggregateFromCollection } = require('../middleware/db');
const router = require('express').Router();

function generateQuery(query, page) {
    return [
        ...query,
        {
            '$sort': {
                'id': 1
            }
        }, {
            '$facet': {
                'results': [
                    {
                        '$skip': Math.max((+page-1)||0, 0)*20
                    }, {
                        '$limit': 20
                    }
                ], 
                'totalCount': [
                    {
                        '$count': 'count'
                    }
                ]
            }
        }, {
            '$project': {
                'results': 1, 
                'totalCount': {
                    '$arrayElemAt': [
                        '$totalCount.count', 0
                    ]
                }, 
                'totalPage': {
                    '$ceil': {
                        '$divide': [
                        {
                            '$arrayElemAt': [
                            '$totalCount.count', 0
                            ]
                        }, 20
                        ]
                    }
                },
                'currentPage': {
                    '$abs': Math.max(+page||1, 1)
                }
            }
        }
    ]
}

router.get('/', async (req, res)=>{
    const { type, name, haveStage, page } = req.query;
    if (type || (name && name.length > 1)) {
        const query = [];
        if(type) {
          type.split('+').forEach(key=>{
            return query.push({
              "$match": {
                "types":{
                  "$elemMatch":{
                   '$eq': key
                  }
                }
              }
            })
          })
        } 
        if(name && name.length>1) {
          query.push({
            "$match": {
              "name": {
                "$regex" : `.*${name}.*`
              }
            }
          })
        }
        
        const aggregateQuery = generateQuery(query, page);
        const data = await aggregateFromCollection('format', aggregateQuery);
    
        res.json(data);
    } else if(+haveStage>=1 && +haveStage<=3){
        const stage=+haveStage;
        const query = [
            {
                '$match': {
                    'evolution_chain.stage2': {
                        '$exists': stage>=2 && true
                    }, 
                    'evolution_chain.stage3': {
                        '$exists': stage>=3 && true
                    }
                }
            }, 
        ];
        const aggregateQuery=generateQuery(query, page);
        const data = await aggregateFromCollection('format', aggregateQuery);
    
        res.json(data);
    } else {
        res.json({message: 'Please povide correct query format!'});
    }

})

module.exports = router;