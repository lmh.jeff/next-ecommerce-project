const { getOneFromCollection } = require('../middleware/db');
const router = require('express').Router();

router.get('/', async (req, res)=>{
    const { id, name } = req.query;
    if (id || name) {
        const query = {};
        if(id) {
          query.id=+id
        } else {
          query.name=name
        }
        
        const data = await getOneFromCollection('format', query);
    
        // const imgMeta = await fetch(`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${data.id}.png`);
        // if(imgMeta.ok) {
        //   const imgBuffer = await imgMeta.buffer();
        //   const base64 = `data:${imgMeta.headers.raw()['content-type']};base64,${imgBuffer.toString('base64')}`;
        //   data.imgurl = base64;
        // }
        
        res.json(data);
    } else {
      res.json({message: 'no query povided!'});
    }
})

module.exports = router;