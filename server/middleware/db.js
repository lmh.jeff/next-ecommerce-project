const Client = require('mongodb').MongoClient;
let db;

async function init() {
    const client = await Client.connect("mongodb://pokemon-db:27017");
    db = client.db('pokemon');
    return true;
}

async function getOneFromCollection(collection, query) {
    const target = db.collection(collection);
    const result = await target.findOne(query);
    return result;
}

async function aggregateFromCollection(collection, query) {
    const target = db.collection(collection);
    const result = await target.aggregate(query).toArray();
    return result;
}

module.exports = {init, getOneFromCollection, aggregateFromCollection};
