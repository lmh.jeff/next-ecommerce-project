const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const routes = require('./routes');
const { init } = require('./middleware/db');
const fetch = require('node-fetch');

const app = express();

app.use(cors());

// Normal express config defaults
app.use(require('morgan')('dev'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get('/',(req, res) => {
    res.json({
        apiEndpoint: {
            '/api/getPokemon': {
                'id': 'exact id of pokemon',
                'name': 'exact name of pokemon',
                '@required': 'id / name'
            },
            '/api/listPokemon': {
                'page': 'page number start from 1',
                'num': 'number of elements(min: 10)',
                '@optional': 'page(default = 1), num(default = 20)'
            },
            '/api/searchPokemon': {
                'page': 'page number start from 1',
                'type': 'use "+" between types if search for more than 1',
                'name': 'min length of 2',
                'haveStage': 'filter pokemons with specific number of stage(s) (1/2/3)',
                '@required': 'type / name / haveStage',
                '@optional': 'page(default = 1)',
            }
        }
    });
});

app.use(routes);

app.get('/test', async(req,res)=> {
    const imgMeta = await fetch("https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/1.png");
    const imgBuffer = await imgMeta.buffer();
    const base64 = `data:${imgMeta.headers.raw()['content-type']};base64,${imgBuffer.toString('base64')}`;
    res.send(base64);
})

app.use(function(req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.json({'errors': {
      message: err.message,
      error: {}
    }});
});

init().then(
    ()=>app.listen(1234, () => {
        console.log('Listening on port 1234...');
    })
);