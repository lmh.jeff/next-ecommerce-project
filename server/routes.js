const router = require('express').Router();
const getPokemonRoutes = require('./api/getPokemon');
const searchPokemonRoutes = require('./api/searchPokemon');
const listPokemonRoutes = require('./api/listPokemon');

router.use('/api/getPokemon', getPokemonRoutes);

router.use('/api/searchPokemon', searchPokemonRoutes);
router.use('/api/listPokemon', listPokemonRoutes);

module.exports = router;