import React from "react";
import Head from "next/head";
import styled from "styled-components/macro";

const SLayout = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
`

const Layout = ({ title, children }) => {
  return (
    <SLayout>
      <Head>
        <title>{title}</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main>{children}</main>
    </SLayout>
  );
};

export default Layout;
