import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import styled from "styled-components/macro";

// const officalArtwork = (id) => {
//     return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
//   };
const SWrapper = styled.div`
  display: flex;
  flex-direction: row;
  width: 15em;
  flex-wrap: wrap;
`;

const SContainer = styled.div`
  display: flex;
  flex-direction: row;
  margin: 3%;
`;

const SImage = styled.img`
  width: 50%;
`;

const SImageContainer = styled.div`
  display: flex;
  width: 30%
`;

const SContentContainer = styled.div`
  width: 70%;
`;

const Cart = () => {
  const cartItems = useSelector((state) => Object.keys(state.cartItems).length);
  // const [image, setImage] = useState(officalArtwork(poke.id));
  const cart = useSelector((state) => state.cartItems);

  // useEffect(()=>{
  //     setImage(officalArtwork(poke.id))
  //   },[poke])

  console.log("cart", cart);

  return (
    <SWrapper>
      {Object.values(cart).map((obj) => {
        return (
          <SContainer>
            <SImageContainer>
              <SImage src={obj.img} />
            </SImageContainer>
            <SContentContainer>{obj.name}</SContentContainer>
          </SContainer>
        );
      })}
    </SWrapper>
  );
};

export default Cart;
