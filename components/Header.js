import styled from "styled-components/macro";
import React from "react";
import {
  LoginOutlined,
  UserOutlined,
  ShoppingCartOutlined,
} from "@ant-design/icons";

import { Popover, Badge } from 'antd';

import 'antd/dist/antd.css'; 
import { useDispatch, useSelector } from 'react-redux';
import Cart from './Cart';

const SHeadeWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-around;
  height: 60px;
  background: #78988f;
  align-items: center;
`;

const SLogoContainer = styled.div`
  display: flex;
  flex-direction: row;
  color: white;
`;

const SAccContainer = styled.div`
  display: flex;
  flex-direction: row;
  color: white;
  width: 300px;
  justify-content: space-around;
  align-items: center;
  
`;

const SSpan = styled.span`
  margin-left: 15px;
  cursor: pointer;
  color: white;
  font-size: 1em;
`;

const SAccButton = styled.div`
    cursor: pointer;
    position: relative;

    &:before {
        content: "";
        position: absolute;
        width: 100%;
        height: 2px;
        bottom: -4px;
        left: 0;
        background-color: #fff;
        visibility: hidden;
        transform: scaleX(0);
        transition: all .3s ease-in-out 0s;
    }
    &:hover {
        &:before {
            visibility: visible; 
            transform: scaleX(1);
        }
    }
   
    
`

const Header = () => {
  const cartItems = useSelector(state => Object.keys(state.cartItems).length)
  // const content = (
  //   <div>
  //     <div>cart</div>
  //     <div>cart</div>
  //     <div>cart</div>
  //     <div>cart</div>
  //   </div>
  // )

  return (
    <SHeadeWrapper>
      <SLogoContainer>
        <div>NextJs Pokede</div>
      </SLogoContainer>
      <SAccContainer>
        <SAccButton onClick={() => console.log('Login')}>
        <LoginOutlined /> 
          <SSpan>Login</SSpan>
        </SAccButton>
          
        <SAccButton onClick={() => console.log('Register')}>
        <UserOutlined /> 
          <SSpan>Register</SSpan>
        </SAccButton>
        <Badge count={cartItems} size="small">
        <Popover content={Cart} title="Shopping Cart" trigger="hover">
          <SSpan onClick={() => console.log('Cart')}>
            <ShoppingCartOutlined />
          </SSpan>
         </Popover>
        </Badge>
         
        
      </SAccContainer>
    </SHeadeWrapper>
  );
};

export default Header;
