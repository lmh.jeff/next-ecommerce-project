import { combineReducers } from 'redux';
import { addToCardReducer } from '../addToCartReducer';
import { costReducer } from '../costReducers';


export default combineReducers({
    cost: costReducer,
    cartItems: addToCardReducer
});
