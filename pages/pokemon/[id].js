import React, { useState, useEffect } from "react";
import Link from "next/link";
import Layout from "../../components/Layout";
import styled from "styled-components/macro";
import { useDispatch, useSelector } from 'react-redux';
import { addToCart } from '../../actions';

const SWrapper = styled.div`
  width: 100%;
  height: 100vh;
  display: flex;
  flex-direction: column;
  align-items: center;
  margin: auto 0;
`;

const SContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 90%;
  width: 50%;
  margin: 1% auto;
`;

const SImage = styled.img`
  width: 100%;
  border: 1px solid #eaeaea;
`;

const SSmallImage = styled.img`
  width: 20%;
  border: 1px solid #eaeaea;
`;

const SDetail = styled.div`
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: flex-start;
  width: 50%;
  padding: 20px;
  font-size: 1.5em;
  width: 25%;
`;

const SImageContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 50%;
`;

const SSmallImageContainer = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  flex-wrap: wrap;
  width: 100%;
`;

const SEvoContainer = styled.div`
  width: 50%;
  margin: 2% auto;
`;

const SEvoDiv = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const SEvoImage = styled.img`
  width: 40%;
  border-radius: 50%;
  border: 6px solid #eaeaea;
`;

const SSpan = styled.span`
  font-size: 20px;
  padding-left: 10%;
`;

const STypeContainer = styled.div`
  display: flex;
  flex-direction: row;
  width: 100%;
  justify-content: space-between;
  align-items: flex-start;
  margin-top: 0.25em;
`

const STypeLabel = styled.div`
  width: 100px;
  padding: 0.25em;
  color: blue;
  border-radius: 14px;

  font-size: 16px;
  text-align: center;
`

// function mainImgSrc(id){
//   return `https://cdn.traction.one/pokedex/pokemon/${id}.png`
// }

const officalArtwork = (id) => {
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${id}.png`;
};

const backShiny = (id) => {
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/${id}.png`;
};

const frontDefault = (id) => {
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/${id}.png`;
};

const frontShiny = (id) => {
  return `https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/${id}.png`;
};

const pokemon = ({ poke }) => {
  const [image, setImage] = useState(officalArtwork(poke.id));
  const dispatch = useDispatch();

  useEffect(()=>{
    setImage(officalArtwork(poke.id))
  },[poke])

  const evoChain = () => {
    return (
      <div style={{ display: "flex" }}>
        {[1, 2, 3].map((e) => {
          return (
            <SEvoDiv>
              {poke.evolution_chain?.[`stage${e}`]?.map((stage) => {
                return (
                  <>
                   
                     
                   <Link href={`/pokemon/${stage.species_id}`} passHref>
                        <SEvoImage
                            src={officalArtwork(stage.species_id)}
                            alt={stage.name}
                          />
                   </Link>
              
                   
                      <h3>
                        {(stage.name).charAt(0).toUpperCase() +
                          (stage.name).slice(1) +
                          " "}
                        #{stage.species_id}
                      </h3>
                      <div>{stage.types}</div>
                 
                  </>
                );
              })}
            </SEvoDiv>
          );
        })}
      </div>
    );
  };


  const haveOtherForm = () => {
    if (poke.other_form) {
      return poke.other_form.map((m) => {
        return (
          <SSmallImage
            src={officalArtwork(m.id)}
            alt={m.name}
            onClick={(e) => setImage(e.target.src)}
          />
        );
      });
    }
  };

  const renderType = () => {
    if (poke.types) {
      return poke.types.map((type) => {
        let color = {};

    switch(type) {
      case 'fire': 
         color = {
            background: 'red',
            color: 'white'
         };
         break;

        case 'flying': 
          color = {
            background: 'linear-gradient(180deg, #3dc7ef 50%, #bdb9b8 50%)',
            color: 'white'
          };
        break;
       
       
        case "normal":
          color = {
            background: '#a4acaf',
            color: 'white'
         };
         break;
        
      
        case "fighting":
          color = {
            background: '#d56723',
            color: 'white'
          };
          break;
      

        case "poison":
          color = {
            background: '#b97fc9',
            color: 'white'
         };
         break;
        
      
      
        case "ground":
          color = {
            background: `linear-gradient(180deg, #f7de3f 50%, #ab9842 50%)`,
            color: 'white'
         };
         break;
        
      
      
        case "rock":
          color = {
            background: '#a38c21',
            color: 'white'
         };
         break;
        
      
      
        case "bug":
          color = {
            background: '#729f3f',
            color: 'white'
         };
         break;
        
      
      
        case "ghost":
          color = {
            background: '#7b62a3',
            color: 'white'
         };
         break;
        
      
      
        case "steel":
          color = {
            background: '#9eb7b8',
            color: 'white'
         };
         break;
        
      
        case "water":
          color = {
            background: '#4592c4',
            color: 'white'
         };
         break;
        
      
      
        case "grass":
          color = {
            background: '#9bcc50',
            color: 'white'
          };
          break;
          
      
      
        case "electric":
          color = {
            background: '#eed535',
            color: 'black'
         };
         break;
        
      
      
        case "psychic":
          color = {
            background: '#f366b9',
            color: 'white'
         };
         break;
        
    
      
        case "ice":
          color = {
            background: '#51c4e7',
            color: 'white'
         };
         break;
        
      
      
        case "dragon":
          color = {
            background: 'linear-gradient(180deg, #53a4cf 50%, #f16e57 50%)',
            color: 'white'
         };
         break;
        
      
      
        case "dark":
          color = {
            background: '#707070',
            color: 'white'
         };
         break;
        
      
      
        case "fairy":
          color = {
            background: '#fdb9e9',
            color: 'white'
         };
         break;
        
      
      
        case "unknown":
          color = {
            background: 'red',
            color: 'white'
         };
         break;
        
      
      
        case "shadow":
          color = {
            background: 'red',
            color: 'white'
         };
         break;
        
      
        default:
          color = {
            background: 'grey',
            color: 'black'
          };
        }
        return (
         
            <STypeLabel key={type} style={color}>{type}</STypeLabel>

        )
      });
    }
  };

  const renderCost = () => {
    const { types, weight, height } = poke;

    const cost = (types.length) * weight + height;

    return <span>Price: ${cost}</span>;
  }

  const renderButton = () => {
    const { types, weight, height, id, name } = poke;

    const cost = (types.length) * weight + height;

    return <span>Price: ${cost}</span>;
  }

  const add = () => {
    const { types, weight, height, id, name } = poke;
    const cost = (types.length) * weight + height;

    let detail = {
      [id]: { 
        name: name,
        height: height, 
        weight: weight,
        types: types,
        cost: cost,
        img: officalArtwork(id)
      }
     
    }

    dispatch(addToCart(detail));
  
  }

  // const renderEvolutionLine = () => {
  //     const mapPokemonparse = JSON.parse(mapPokemon);
  //     if(mapPokemonparse.length >= 2) {
  //       return mapPokemonparse.map(image=> {
  //           return <SSmallImage key={image} src={image} />
  //       })
  //     } else {
  //         return (
  //             <SSmallImage src={mapPokemonparse[0]} />
  //         );
  //     }
  // }

  // console.log("evoChain", evoChain)

  return (
    <SWrapper>
      <SContainer>
        <SImageContainer>
          <SImage src={image} alt={poke.name} />
          <SSmallImageContainer>
            <SSmallImage
              src={officalArtwork(poke.id)}
              alt={poke.name}
              onClick={(e) => setImage(e.target.src)}
            />
            <SSmallImage
              src={backShiny(poke.id)}
              alt={poke.name}
              onClick={(e) => setImage(e.target.src)}
            />
            <SSmallImage
              src={frontDefault(poke.id)}
              alt={poke.name}
              onClick={(e) => setImage(e.target.src)}
            />
            <SSmallImage
              src={frontShiny(poke.id)}
              alt={poke.name}
              onClick={(e) => setImage(e.target.src)}
            />
            {haveOtherForm()}
          </SSmallImageContainer>
        </SImageContainer>
        <SDetail>
          {<span>Name: {poke.name}</span>}
          <span>Height: {poke.height}</span>
          <span>Weight: {poke.weight}  lbs</span>
          <STypeContainer>
            {renderType()}
          </STypeContainer>
            {renderCost()}

            <button onClick={() => add()}>Add to cart</button>
    
        </SDetail>

      </SContainer>
      <SEvoContainer>
        <SSpan>Evolution</SSpan>
     
        {evoChain()}
      </SEvoContainer>

      <Link href={"/"}>
        <button>Home</button>
      </Link>
    </SWrapper>
  );
};

export default pokemon;

export async function getStaticPaths() {
  const paths = new Array(12000).fill(0).map((e, index) => ({
    params: { id: (index + 1).toString() },
  }));

  // We'll pre-render only these paths at build time.
  // { fallback: false } means other routes should 404.
  return { paths, fallback: false };
}

export const getStaticProps = async ({ params }) => {
  const id = params.id;
  // try {
  // const poke = await P.getPokemonByName(id);
  const res = await fetch(`http://pokemon-server:1234/api/getPokemon?id=${id}`);
  // `https://pokeapi.glitch.me/v1/pokemon/${id}`

  const poke = await res.json();



  return { props: { poke: poke } };
};
