import App from 'next/app';

import '../styles/globals.css';
import { Provider } from 'react-redux';
import store from '../store/store'
import { createWrapper } from 'next-redux-wrapper';

import Header from "../components/Header";

function MyApp({ Component, pageProps }) {
  // const store = useStore(pageProps.initialReduxState);

  return (
          <Provider store={store}>
            <Header />
            <Component {...pageProps} />
          </Provider>
    
  )

}

// class MyApp extends App {
//   render() {
//     const { Component, pageProps } = this.props;

//     return (
//       <Provider store={store}>
//              <Header />
//              <Component {...pageProps} />
//       </Provider>
//     )
//   }
// }

// const makestore = () => store;
// const wrapper = createWrapper(makestore);

export default MyApp;
